vim.cmd([[
	filetype plugin indent on
]])

-- theme
vim.opt.termguicolors = true
vim.o.background = "dark"

-- misc
vim.opt.backspace = { "eol", "start", "indent" }
vim.opt.encoding = "utf-8"
vim.opt.syntax = "enable"

-- indentation
local indent_size = 4
vim.opt.autoindent = true
vim.opt.expandtab = false
vim.opt.shiftwidth = indent_size
vim.opt.smartindent = true
vim.opt.softtabstop = indent_size
vim.opt.tabstop = indent_size

-- ui
vim.opt.cursorline = true
vim.opt.laststatus = 2
vim.opt.list = true
vim.opt.listchars = {
	tab = "»-",
	trail = "·",
	lead = "·",
	extends = "→",
	precedes = "←",
	nbsp = "×",
	eol = "¬",
}
vim.opt.mouse = "a"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 18
vim.opt.showmode = false
vim.opt.sidescrolloff = 3 -- Lines to scroll horizontally
vim.opt.signcolumn = "yes"
vim.opt.splitbelow = true -- Open new split below
vim.opt.splitright = true -- Open new split to the right
vim.opt.wrap = false
vim.opt.colorcolumn = "80"

vim.opt.clipboard:append("unnamedplus")

vim.filetype.add({
	extension = {
		ixx = "cpp"
	}
})

