local map = vim.keymap.set

map("v", "L", ">gv", { desc = "Indent selection" })
map("v", "H", "<gv", { desc = "Unindent selection" })

map("v", "K", ":m '<-2<cr>gv=gv", { desc = "Shift lines up" })
map("v", "J", ":m '>+1<cr>gv=gv", { desc = "Shift lines down" })
