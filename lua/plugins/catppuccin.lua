return {
	"catppuccin/nvim",
	name = "catppuccin",
	priority = 1000,
	opts = {
		noice = true,
		native_lsp = {
			enabled = true,
			virtual_text = {
				errors = { "italic" },
				hints = { "italic" },
				warnings = { "italic" },
				information = { "italic" },
				ok = { "italic" },
			},
			underlines = {
				errors = { "underline" },
				hints = { "underline" },
				warnings = { "underline" },
				information = { "underline" },
				ok = { "underline" },
			},
			inlay_hints = {
				background = true,
			},
		}
	},
	config = function(_, _opts)
		vim.cmd.colorscheme("catppuccin-macchiato")
		require("catppuccin").setup(_opts)
	end
}
