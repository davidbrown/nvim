return {
	"nvim-telescope/telescope.nvim",
	dependencies = { "nvim-lua/plenary.nvim" },
	opts = {
		defaults = {
			path_display = { "smart" },
			mappings = {
				i = {
					["<C-k"] = require("telescope.actions").move_selection_previous,
					["<C-j"] = require("telescope.actions").move_selection_next,
				}
			}
		}
	},
	keys = {
		{"<leader>ff", require("telescope.builtin").find_files, desc = "Find files"},
		{"<leader>fg", require("telescope.builtin").live_grep, desc = "Find string"},
		{"<leader>fb", require("telescope.builtin").buffers, desc = "Find buffer"}
	}
}
