return {
	"neovim/nvim-lspconfig",
	dependencies = {
		"williamboman/mason.nvim",
		"williamboman/mason-lspconfig.nvim",
		"nvimtools/none-ls.nvim"
	},
	event = { "BufReadPre", "BufNewFile" },
	config = function()
		require("mason").setup()
		require("mason-lspconfig").setup({
			ensure_installed = {
				"clangd",
				"cmake",
				"lua_ls",
				"pylsp"
			},
			automatic_installation = true
		})

		local diag_config = {
			virtual_text = false,
			update_in_insert = true,
			underline = true,
			severity_sort = true,
			signs = {
				text = {
					[vim.diagnostic.severity.ERROR] = '',
					[vim.diagnostic.severity.WARN] = '',
					[vim.diagnostic.severity.INFO] = '',
					[vim.diagnostic.severity.HINT] = ''
				}
			},
			float = {
				focusable = true,
				style = "minimal",
				border = "rounded",
				source = "always",
				header = "",
				prefix = ""
			}
		}

		vim.diagnostic.config(diag_config)

		local on_attach = function(_, bufnr)
			local lsp_map = function(key, cmd, desc)
				vim.keymap.set("n", key, cmd, {
					noremap = true,
					silent = true,
					buffer = bufnr,
					desc = desc
				})
			end

			lsp_map("<leader>gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", "Go to declaration")
			lsp_map("<leader>gd", "<cmd>lua vim.lsp.buf.definition()<CR>", "Go to definition")
			lsp_map("<leader>gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", "Go to implementation")
			lsp_map("<leader>gr", require("telescope.builtin").lsp_references, "Go to references")

			vim.api.nvim_buf_create_user_command(bufnr, "Format", function(_)
				vim.lsp.buf.format()
			end, { desc = "Format current buffer with LSP" })

			lsp_map("<leader>lf", "<cmd>Format<CR>", "Format")
			lsp_map("<leader>la", "<cmd>lua vim.lsp.buf.code_action()<CR>", "Code action")
			lsp_map("<leader>ldw", "<cmd>Telescope diagnostics<cr>", "Show workspace diagnostics")
			lsp_map("<leader>ldb", "<cmd>Telescope diagnostics bufnr=0<cr>", "Show buffer diagnostics")
			lsp_map("<leader>ldl", "<cmd>lua vim.diagnostic.open_float()<CR>", "Show line diagnostics")
			lsp_map("<leader>lj", "<cmd>lua vim.diagnostic.goto_next()<CR>", "Go to next diagnostic")
			lsp_map("<leader>lk", "<cmd>lua vim.diagnostic.goto_prev()<CR>", "Go to previous diagnostic")
			lsp_map("<leader>lr", "<cmd>lua vim.lsp.buf.rename()<CR>", "Rename")
			lsp_map("<leader>lh", "<cmd>lua vim.lsp.buf.hover()<CR>", "Show documentation")
		end

		local capabilities = vim.lsp.protocol.make_client_capabilities()
		capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

		local lspconfig = require("lspconfig")

		lspconfig.clangd.setup({
			on_attach = on_attach,
			capabilities = capabilities
		})
		lspconfig.cmake.setup({
			on_attach = on_attach,
			capabilities = capabilities
		})
		lspconfig.lua_ls.setup({
			on_attach = on_attach,
			capabilities = capabilities,
			settings = {
				Lua = {
					diagnostics = {
						globals = { "vim" }
					},
					workspace = {
						library = {
							vim.env.VRUNTIME
						}
					}
				}
			}
		})
		lspconfig.pylsp.setup({
			on_attach = on_attach,
			capabilities = capabilities
		})
	end
}
