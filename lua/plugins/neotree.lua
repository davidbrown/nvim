return {
	"nvim-neo-tree/neo-tree.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons",
		"MunifTanjim/nui.nvim",
	},
	opts = {
		filesystem = {
			filtered_items = {
				hide_dotfiles = false,
			},
		},
		source_selector = {
			winbar = true,
			statusline = false
		}
	},
	keys = {
		{ "<leader>et", "<cmd>Neotree toggle<cr>", desc = "Toggle file explorer" },
		{ "<leader>ef", "<cmd>Neotree focus<cr>",  desc = "Focus file explorer" },
		{ "<leader>er", "<cmd>Neotree reveal<cr>", desc = "Show file in explorer" },
		{ "<leader>eb", "<cmd>Neotree reveal<cr>", desc = "Show file in explorer" }
	}
}
