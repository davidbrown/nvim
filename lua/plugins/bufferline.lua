return {
	"akinsho/bufferline.nvim",
	opts = {
		options = {
			offsets = {
				{
					filetype = "neo-tree",
					text = "Project Explorer",
					highlight = "Directory",
					separator = true
				}
			},
			separator_style = "slant"
		}
	},
}
