return {
	"nvim-treesitter/nvim-treesitter",
	event = { "BufReadPre", "BufNewFile" },
	build = ":TSUpdate",
	dependencies = {
		"nvim-treesitter/nvim-treesitter-textobjects",
	},
	init = function(plugin)
		require("lazy.core.loader").add_to_rtp(plugin)
		require("nvim-treesitter.query_predicates")
	end,
	opts = {
		ensure_installed = {
			"bash",
			"c",
			"cpp",
			"doxygen",
			"lua",
			"markdown",
			"markdown_inline",
			"python",
			"regex",
			"vim",
			"vimdoc"
		},
		highlight = { enable = true }
	},
}
